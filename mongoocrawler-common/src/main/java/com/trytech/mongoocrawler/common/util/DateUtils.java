package com.trytech.mongoocrawler.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hp on 2017-5-15.
 */
public class DateUtils {
    public static String now(String pattern){
        Date now = new Date();
        SimpleDateFormat sf = new SimpleDateFormat(pattern);
        return sf.format(now);
    }
}
