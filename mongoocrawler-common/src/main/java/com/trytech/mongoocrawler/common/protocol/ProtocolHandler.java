package com.trytech.mongoocrawler.common.protocol;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年04月10日
 */
public abstract class ProtocolHandler<S extends AbstractProtocol> {
    public AbstractProtocol handle(ProtocolFilterChain protocolFilterChain, AbstractProtocol abstractProtocol) throws Exception {
        AbstractProtocol protocol = null;
        if (checkProtocol(abstractProtocol)) {
            protocol = doHandle(protocolFilterChain, (S) abstractProtocol);
            if (protocol != null) {
                protocolFilterChain.resetIndex();
                return protocol;
            }

        }
        return protocolFilterChain.doFilter(abstractProtocol);
    }

    protected abstract AbstractProtocol doHandle(ProtocolFilterChain filterChain, S s) throws Exception;

    protected abstract boolean checkProtocol(AbstractProtocol abstractProtocol);

}
