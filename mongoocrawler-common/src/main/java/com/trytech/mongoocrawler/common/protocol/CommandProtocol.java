package com.trytech.mongoocrawler.common.protocol;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by coliza on 2018/3/24.
 */
public class CommandProtocol extends AbstractProtocol {

    private Command command;

    private Object data;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public ProtocolType getType() {
        return ProtocolType.COMMAND;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    @Override
    public String toJSONString() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", getType().val());
        jsonObject.put("command", getCommand().getValue());
        return jsonObject.toJSONString();
    }

    public enum Command {
        GET_URL((byte) 1), HEARTBEAT((byte) 2), CONTINUE((byte) 3), MONITOR((byte) 4), CRAWL((byte) 5), OVER((byte) 6);
        private byte value;

        Command(byte value) {
            this.value = value;
        }

        public byte getValue() {
            return value;
        }

        public boolean equalsTo(Command command) {
            return getValue() == command.getValue();
        }
    }


}
