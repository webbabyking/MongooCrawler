package com.trytech.mongoocrawler.server.common.queue;

/**
 * fetcher写入事件
 */
public class FetcherEvent<T> {
    protected T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
