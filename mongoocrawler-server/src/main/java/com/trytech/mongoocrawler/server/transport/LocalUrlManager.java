package com.trytech.mongoocrawler.server.transport;

/**
 * Created by coliza on 2017/4/9.
 */
public class LocalUrlManager extends UrlManager{
    @Override
    public void pushUrl(UrlParserPair urlParserPair){
        if(isUnique(urlParserPair.getUrl())){
            try {
                urlQueue.put(urlParserPair);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void pushUrls(UrlParserPair[] urlParserPairs){
        for(UrlParserPair urlParserPair:urlParserPairs) {
            pushUrl(urlParserPair);
        }
    }

    @Override
    public UrlParserPair fetchUrl() {
        UrlParserPair url = null;
        try {
            url = urlQueue.take();
            return url;
        } catch (InterruptedException e) {
            return null;
        }
    }
}
