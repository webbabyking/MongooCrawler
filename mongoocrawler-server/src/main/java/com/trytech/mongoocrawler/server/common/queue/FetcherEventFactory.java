package com.trytech.mongoocrawler.server.common.queue;

import com.lmax.disruptor.EventFactory;

/**
 * Created by coliza on 2017/4/8.
 */
public abstract class FetcherEventFactory<FetcherEvent> implements EventFactory<FetcherEvent> {

    public enum EVENT_TYPE{
        URL_EVENT("URL"),RESULT_EVENT("WEBRESULT");
        private String value;
        EVENT_TYPE(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
        public boolean equals(EVENT_TYPE eventType){
            return eventType.getValue().equals(getValue());
        }
    }
}
