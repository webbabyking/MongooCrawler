package com.trytech.mongoocrawler.server.entity;

/**
 * 京东图书实体类
 */
public class JDItem {
    //书名
    private String name="";
    //网址
    private String url="";
    //作者
    private String author="";
    //语言
    private String language="中文";
    //出版社
    private String agent="";
    //售价
    private Float price = 0.00f;
    //isbn
    private String isbn="";
    //商品编号
    private String no="";
    //评价数
    private String commentCount="";
    //好评率
    private Short goodCommentRate = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(String commentCount) {
        this.commentCount = commentCount;
    }

    public Short getGoodCommentRate() {
        return goodCommentRate;
    }

    public void setGoodCommentRate(Short goodCommentRate) {
        this.goodCommentRate = goodCommentRate;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
