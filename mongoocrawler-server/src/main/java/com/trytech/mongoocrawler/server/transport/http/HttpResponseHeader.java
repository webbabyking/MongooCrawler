package com.trytech.mongoocrawler.server.transport.http;

import java.util.HashSet;

/**
 * Created by coliza on 2017/7/31.
 */
public class HttpResponseHeader extends HttpHeader {
    public final static String ACCEPT_RANGES = "Accept-Ranges";
    public final static String AGE = "Age";
    public final static String ALLOW = "Allow";
    public final static String CACHE_CHONTROL = "Cache-Control";
    public final static String CONTENT_ENCODING = "Content-Encoding";
    public final static String CONTENT_LANGUAGE = "Content-Language";
    public final static String CONTENT_LENGTH = "Content-Length";
    public final static String CONTENT_LOCATION = "Content-Location";
    public final static String CONTENT_MD5 = "Content-MD5";
    public final static String CONTENT_RANGE = "Content-Range";
    public final static String CONTENT_TYPE = "Content-Type";
    public final static String DATE = "Date";
    public final static String ETAG = "ETag";
    public final static String EXPIRES = "Expires";
    public final static String LAST_MODIFIED = "Last-Modified";
    public final static String LOCATION = "Location";
    public final static String PRAGMA = "Pragma";
    public final static String PROXY_AUTHENTICATE = "Proxy-Authenticate";
    public final static String REFRESH = "refresh";
    public final static String RETRY_AFTER = "Retry-After";
    public final static String SERVER = "Server";
    public final static String SET_COOKIE = "Set-Cookie";
    public final static String TRAILER = "Trailer";
    public final static String TRANSFER_ENCODING = "Transfer-Encoding";
    public final static String VARY = "Vary";
    public final static String VIA = "Via";
    public final static String WARNING = "Warning";
    public final static String WWW_AUTHENTICATE = "WWW-Authenticate";
    public final static HashSet HEADER_FIELDS_MAP = new HashSet<String>();
    static {
        HEADER_FIELDS_MAP.add(ACCEPT_RANGES);
        HEADER_FIELDS_MAP.add(AGE);
        HEADER_FIELDS_MAP.add(ALLOW);
        HEADER_FIELDS_MAP.add(CACHE_CHONTROL);
        HEADER_FIELDS_MAP.add(CONTENT_ENCODING);
        HEADER_FIELDS_MAP.add(CONTENT_LANGUAGE);
        HEADER_FIELDS_MAP.add(CONTENT_LENGTH);
        HEADER_FIELDS_MAP.add(CONTENT_LOCATION);
        HEADER_FIELDS_MAP.add(CONTENT_MD5);
        HEADER_FIELDS_MAP.add(CONTENT_RANGE);
        HEADER_FIELDS_MAP.add(CONTENT_TYPE);
        HEADER_FIELDS_MAP.add(DATE);
        HEADER_FIELDS_MAP.add(ETAG);
        HEADER_FIELDS_MAP.add(EXPIRES);
        HEADER_FIELDS_MAP.add(LAST_MODIFIED);
        HEADER_FIELDS_MAP.add(LOCATION);
        HEADER_FIELDS_MAP.add(PRAGMA);
        HEADER_FIELDS_MAP.add(PROXY_AUTHENTICATE);
        HEADER_FIELDS_MAP.add(REFRESH);
        HEADER_FIELDS_MAP.add(RETRY_AFTER);
        HEADER_FIELDS_MAP.add(SERVER);
        HEADER_FIELDS_MAP.add(SET_COOKIE);
        HEADER_FIELDS_MAP.add(TRAILER);
        HEADER_FIELDS_MAP.add(TRANSFER_ENCODING);
        HEADER_FIELDS_MAP.add(VARY);
        HEADER_FIELDS_MAP.add(VIA);
        HEADER_FIELDS_MAP.add(WARNING);
        HEADER_FIELDS_MAP.add(WWW_AUTHENTICATE);
    }
    private int statusCode;//状态码
    private String message;//消息
    private String acceptRanges;
    private String age;
    private String allow;
    private String cacheChontrol;
    private String contentEncoding;
    private String contentLanguage;
    private String contentLength;
    private String contentLocation;
    private String contentMD5;
    private String contentRange;
    private String contentType;
    private String date;
    private String eTag;
    private String expires;
    private String lastModified;
    private String location;
    private String pragma;
    private String proxyAuthenticate;
    private String refresh;
    private String retryAfter;
    private String server;
    private String setCookie;
    private String trailer;
    private String transferEncoding;
    private String vary;
    private String via;
    private String warning;
    private String wwwAuthenticate;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAcceptRanges() {
        return acceptRanges;
    }

    public void setAcceptRanges(String acceptRanges) {
        this.acceptRanges = acceptRanges;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAllow() {
        return allow;
    }

    public void setAllow(String allow) {
        this.allow = allow;
    }

    public String getCacheChontrol() {
        return cacheChontrol;
    }

    public void setCacheChontrol(String cacheChontrol) {
        this.cacheChontrol = cacheChontrol;
    }

    public String getContentEncoding() {
        return contentEncoding;
    }

    public void setContentEncoding(String contentEncoding) {
        this.contentEncoding = contentEncoding;
    }

    public String getContentLanguage() {
        return contentLanguage;
    }

    public void setContentLanguage(String contentLanguage) {
        this.contentLanguage = contentLanguage;
    }

    public String getContentLength() {
        return contentLength;
    }

    public void setContentLength(String contentLength) {
        this.contentLength = contentLength;
    }

    public String getContentLocation() {
        return contentLocation;
    }

    public void setContentLocation(String contentLocation) {
        this.contentLocation = contentLocation;
    }

    public String getContentMD5() {
        return contentMD5;
    }

    public void setContentMD5(String contentMD5) {
        this.contentMD5 = contentMD5;
    }

    public String getContentRange() {
        return contentRange;
    }

    public void setContentRange(String contentRange) {
        this.contentRange = contentRange;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String geteTag() {
        return eTag;
    }

    public void seteTag(String eTag) {
        this.eTag = eTag;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPragma() {
        return pragma;
    }

    public void setPragma(String pragma) {
        this.pragma = pragma;
    }

    public String getProxyAuthenticate() {
        return proxyAuthenticate;
    }

    public void setProxyAuthenticate(String proxyAuthenticate) {
        this.proxyAuthenticate = proxyAuthenticate;
    }

    public String getRefresh() {
        return refresh;
    }

    public void setRefresh(String refresh) {
        this.refresh = refresh;
    }

    public String getRetryAfter() {
        return retryAfter;
    }

    public void setRetryAfter(String retryAfter) {
        this.retryAfter = retryAfter;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getSetCookie() {
        return setCookie;
    }

    public void setSetCookie(String setCookie) {
        this.setCookie = setCookie;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getTransferEncoding() {
        return transferEncoding;
    }

    public void setTransferEncoding(String transferEncoding) {
        this.transferEncoding = transferEncoding;
    }

    public String getVary() {
        return vary;
    }

    public void setVary(String vary) {
        this.vary = vary;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getWarning() {
        return warning;
    }

    public void setWarning(String warning) {
        this.warning = warning;
    }

    public String getWwwAuthenticate() {
        return wwwAuthenticate;
    }

    public void setWwwAuthenticate(String wwwAuthenticate) {
        this.wwwAuthenticate = wwwAuthenticate;
    }
}
