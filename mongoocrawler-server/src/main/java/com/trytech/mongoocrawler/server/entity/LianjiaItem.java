package com.trytech.mongoocrawler.server.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * 链家实体
 */
public class LianjiaItem {
    private Integer id;
    private String title;
    private String location;
    private String type;
    private String floorSpace;
    private Float price;
    private Integer unitPrice;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFloorSpace() {
        return floorSpace;
    }

    public void setFloorSpace(String floorSpace) {
        this.floorSpace = floorSpace;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Integer unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Map<String,Object> toMap(){
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("title",title);
        map.put("location", location);
        map.put("type",type);
        map.put("floorSpace",floorSpace);
        map.put("price",price);
        map.put("unitPrice", unitPrice);
        return map;
    }
}
