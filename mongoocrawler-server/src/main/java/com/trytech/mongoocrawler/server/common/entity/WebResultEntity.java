package com.trytech.mongoocrawler.server.common.entity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by coliza on 2017/3/21.
 */
public class WebResultEntity {
    private String source;
    private String html;
    private Date date;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(String date) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            this.date = formatter.parse(date);
        } catch (ParseException e) {
            this.date = new Date();
        }
    }
}
