package com.trytech.mongoocrawler.server.common.queue;

/**
 * Created by coliza on 2016/11/8.
 */
public abstract class CrawlerWorkHandler{
    //hash前缀名
    protected final static String HASH_PREFIX = "CRAWLER_EVENT_";
    public abstract String getHashKey();
}
