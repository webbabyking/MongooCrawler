package com.trytech.mongoocrawler.server.transport.handler;

import com.trytech.mongoocrawler.common.protocol.AbstractProtocol;
import com.trytech.mongoocrawler.common.protocol.CommandProtocol;
import com.trytech.mongoocrawler.common.protocol.ProtocolFilterChain;
import com.trytech.mongoocrawler.common.protocol.UrlProtocol;
import com.trytech.mongoocrawler.monitor.protocol.MonitorProtocol;
import com.trytech.mongoocrawler.server.CrawlerContext;
import com.trytech.mongoocrawler.server.DistributedCrawlerSession;
import com.trytech.mongoocrawler.server.parser.lianjia.LianjiaHtmlParser;
import com.trytech.mongoocrawler.server.transport.UrlParserPair;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年07月01日
 */
public class CommandProtocolHandler extends ServerProtocolHandler {

    public CommandProtocolHandler(CrawlerContext crawlerContext) {
        super(crawlerContext);
    }

    @Override
    public AbstractProtocol doHandle(ProtocolFilterChain filterChain, AbstractProtocol protocol) throws Exception {
        CommandProtocol.Command command = ((CommandProtocol) protocol).getCommand();
        if (command.equalsTo(CommandProtocol.Command.GET_URL)) {
            //获取session
            DistributedCrawlerSession crawlerSession = (DistributedCrawlerSession) crawlerContext.getSession(protocol
                    .getSessionId());

            UrlParserPair urlParserPair = crawlerSession.fetchUrl();
            //从url池中获取url
            UrlProtocol urlProtocol = new UrlProtocol();
            urlProtocol.setTraceId("001");
            urlProtocol.setUrl(urlParserPair.getUrl());
            return urlProtocol;
        } else if (command.equalsTo(CommandProtocol.Command.HEARTBEAT)) {
            //如果是心跳
            return null;
        } else if (command.equalsTo(CommandProtocol.Command.CRAWL)) {
            //如果是爬取任务
            DistributedCrawlerSession crawlerSession = (DistributedCrawlerSession) crawlerContext.getSession(protocol
                    .getSessionId());
            String url = ((CommandProtocol) protocol).getData().toString();
            UrlParserPair urlParserPair = new UrlParserPair(url, new LianjiaHtmlParser());
            crawlerSession.pushUrl(urlParserPair);

            //从url池中获取url
            MonitorProtocol monitorProtocol = new MonitorProtocol();
            monitorProtocol.setTraceId(protocol.getTraceId());
            monitorProtocol.setSessionId(crawlerSession.getSessionId());
            return monitorProtocol;
        }
        return filterChain.doFilter(protocol);
    }

    @Override
    protected boolean checkProtocol(AbstractProtocol abstractProtocol) {
        return abstractProtocol instanceof CommandProtocol;
    }
}
