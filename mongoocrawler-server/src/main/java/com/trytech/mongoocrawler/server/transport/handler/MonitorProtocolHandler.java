package com.trytech.mongoocrawler.server.transport.handler;

import com.trytech.mongoocrawler.common.protocol.AbstractProtocol;
import com.trytech.mongoocrawler.common.protocol.ProtocolFilterChain;
import com.trytech.mongoocrawler.monitor.bean.MonitorData;
import com.trytech.mongoocrawler.monitor.protocol.MonitorProtocol;
import com.trytech.mongoocrawler.server.CrawlerContext;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年07月01日
 */
public class MonitorProtocolHandler extends ServerProtocolHandler {
    public MonitorProtocolHandler(CrawlerContext crawlerContext) {
        super(crawlerContext);
    }

    @Override
    public AbstractProtocol doHandle(ProtocolFilterChain filterChain, AbstractProtocol abstractProtocol) throws Exception {
        //从url池中获取url
        MonitorProtocol monitorProtocol = new MonitorProtocol();
        monitorProtocol.setTraceId(abstractProtocol.getTraceId());
        monitorProtocol.setSessionId(abstractProtocol.getSessionId());
        MonitorData monitorData = new MonitorData();
        monitorData.setRunStatus("运行中");
        monitorData.setMode(0);
        monitorData.setModeLabel("单机");
        monitorData.setCrawlerCount(1);
        monitorProtocol.setMonitorData(monitorData);
        return monitorProtocol;
    }

    @Override
    protected boolean checkProtocol(AbstractProtocol abstractProtocol) {
        return abstractProtocol instanceof MonitorProtocol;
    }
}
