package com.trytech.mongoocrawler.server.pipeline;

import com.trytech.mongoocrawler.common.util.DateUtils;
import com.trytech.mongoocrawler.server.CrawlerContext;
import com.trytech.mongoocrawler.server.common.db.CrawlerDataSource;
import com.trytech.mongoocrawler.server.common.db.MySqlDruidDataSource;
import com.trytech.mongoocrawler.server.entity.JDItem;

import java.sql.SQLException;

/**
 * Created by coliza on 2017/6/15.
 */
public class JDPipeline extends AbstractPipeline<JDItem> {
    @Override
    public void store(JDItem item) throws SQLException, ClassNotFoundException {
        String sql = "insert into jd(`NAME`,`PRICE`,`AUTHOR`,`LANGUAGE`,`AGENT`,`ISBN`,`NO`,`URL`,`CREATE_TIME`) values(\"" + item.getName() + "\"," + item.getPrice().toString() + ",\"" + item.getAuthor() +
                "\",\"" + item.getLanguage() + "\",\"" + item.getAgent() + "\",\"" + item.getIsbn() + "\",\"" + item.getNo() + "\",\"" + item.getUrl() + "\",\""+ DateUtils.now("yyyy-MM-dd HH:mm:ss")+"\")";
        MySqlDruidDataSource dataSource = (MySqlDruidDataSource)getDataSource();
        dataSource.insert(sql);
    }

    @Override
    public CrawlerDataSource getDataSource() {
        return CrawlerContext.getConfig().getConfigBean().getDataSource(DATASOURCE_NAME);
    }
}
