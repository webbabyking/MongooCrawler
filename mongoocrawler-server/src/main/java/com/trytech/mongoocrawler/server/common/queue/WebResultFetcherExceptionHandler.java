package com.trytech.mongoocrawler.server.common.queue;

import com.lmax.disruptor.ExceptionHandler;
/**
 * Created by coliza on 2017/4/8.
 */
public class WebResultFetcherExceptionHandler implements ExceptionHandler<WebResultFetcherEvent> {

    @Override
    public void handleEventException(Throwable ex, long sequence, WebResultFetcherEvent event) {
        System.out.println(ex.getStackTrace());
        System.out.println(event.getData());
    }

    @Override
    public void handleOnStartException(Throwable ex) {
        System.out.println(ex.getStackTrace());
    }

    @Override
    public void handleOnShutdownException(Throwable ex) {
        System.out.println(ex.getStackTrace());
    }
}
