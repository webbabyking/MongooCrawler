package com.trytech.mongoocrawler.server.transport.http;

import com.trytech.mongoocrawler.server.parser.HtmlParser;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by coliza on 2016/11/3.
 */
public class CrawlerHttpRequest {
    private String sessionId;
    private URL url;
    private Map<String,Object> params;
    private Map<String,String> headers = new HashMap<String, String>();
    private HtmlParser parser;

    public CrawlerHttpRequest(URL url, Map<String,Object> params,HtmlParser parser) {
        this.url = url;
        this.params = params;
        this.parser = parser;
    }

    public CrawlerHttpRequest(String sessionId, URL url, Map<String,Object> params,HtmlParser parser) {
        this.url = url;
        this.params = params;
        this.parser = parser;
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public HtmlParser getParser() {
        return parser;
    }

    public void setParser(HtmlParser parser) {
        this.parser = parser;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void addHeader(String key, String value) {
        headers.put(key,value);
    }
    public void removeHeader(String key) {
        headers.remove(key);
    }
}
