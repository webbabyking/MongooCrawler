package com.trytech.mongoocrawler.server.common.queue;

import com.lmax.disruptor.WorkHandler;
import com.trytech.mongoocrawler.server.CrawlerSession;
import com.trytech.mongoocrawler.server.transport.UrlParserPair;
import com.trytech.mongoocrawler.server.transport.http.UrlResult;

/**
 * Created by coliza on 2017/4/8.
 */
public class UrlResultFetcherWorkHandler extends FetcherWorkHandler implements WorkHandler<UrlResultFetcherEvent> {

    public UrlResultFetcherWorkHandler(DisruptorContext disruptorContext){
        super(disruptorContext);
    }

    @Override
    public void onEvent(UrlResultFetcherEvent event) throws Exception {
        CrawlerSession session = disruptorContext.getCrawlerSession();
        UrlResult urlResult = event.getData();
        session.pushUrl(new UrlParserPair(urlResult.getUrl(),urlResult.getParser()));
    }
}
