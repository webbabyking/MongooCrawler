package com.trytech.mongoocrawler.server.common.queue;

import com.lmax.disruptor.RingBuffer;
import com.trytech.mongoocrawler.server.transport.http.UrlResult;

/**
 * Created by hp on 2017-1-25.
 */
public class UrlFetcherEventProducer extends FetcherEventProducer<UrlResultFetcherEvent,UrlResult> {
    public UrlFetcherEventProducer(RingBuffer<UrlResultFetcherEvent> ringBuffer) {
        super(ringBuffer);
    }
}
