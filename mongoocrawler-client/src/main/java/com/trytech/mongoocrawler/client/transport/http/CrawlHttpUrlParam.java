package com.trytech.mongoocrawler.client.transport.http;

import org.apache.http.NameValuePair;

/**
 * Created by hp on 2017-1-24.
 */
public class CrawlHttpUrlParam implements NameValuePair {
    public String key;
    public String value;
    public CrawlHttpUrlParam(String key,String value){
        this.key = key;
        this.value = value;
    }
    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getValue() {
        return null;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
