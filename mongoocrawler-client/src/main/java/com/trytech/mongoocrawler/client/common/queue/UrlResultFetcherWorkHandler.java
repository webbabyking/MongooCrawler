package com.trytech.mongoocrawler.client.common.queue;

import com.lmax.disruptor.WorkHandler;

/**
 * Created by coliza on 2017/4/8.
 */
public class UrlResultFetcherWorkHandler extends FetcherWorkHandler implements WorkHandler<UrlResultFetcherEvent> {

    public UrlResultFetcherWorkHandler(DisruptorContext disruptorContext){
        super(disruptorContext);
    }

    @Override
    public void onEvent(UrlResultFetcherEvent event) throws Exception {
//        CrawlerSession session = disruptorContext.getCrawlerSession();
//        UrlResult urlResult = event.getData();
//        session.pushUrl(new UrlParserPair(urlResult.getUrl(),urlResult.getParser()));
    }
}
