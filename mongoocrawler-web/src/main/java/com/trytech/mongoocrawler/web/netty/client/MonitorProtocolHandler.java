package com.trytech.mongoocrawler.web.netty.client;

import com.trytech.mongoocrawler.common.protocol.AbstractProtocol;
import com.trytech.mongoocrawler.common.protocol.ProtocolFilterChain;
import com.trytech.mongoocrawler.common.protocol.ProtocolHandler;
import com.trytech.mongoocrawler.monitor.protocol.MonitorProtocol;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年04月10日
 */
public class MonitorProtocolHandler extends ProtocolHandler {
    private AbstractProtocol protocol;

    public AbstractProtocol getProtocol() {
        return protocol;
    }

    @Override
    public AbstractProtocol doHandle(ProtocolFilterChain filterChain, AbstractProtocol abstractProtocol) throws Exception {
        this.protocol = abstractProtocol;
        if (abstractProtocol != null) {
            return abstractProtocol;
        }
        return null;
    }

    @Override
    protected boolean checkProtocol(AbstractProtocol abstractProtocol) {
        return abstractProtocol instanceof MonitorProtocol;
    }
}
