package com.trytech.mongoocrawler.web.controller;

import com.trytech.mongoocrawler.monitor.bean.ClientInfo;
import com.trytech.mongoocrawler.monitor.bean.CrawlerData;
import com.trytech.mongoocrawler.monitor.protocol.MonitorProtocol;
import com.trytech.mongoocrawler.web.vo.ServerConfVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collection;
import java.util.List;

/**
 * Created by coliza on 2018/6/2.
 */
@Controller
@RequestMapping("/config")
public class ConfigController extends BaseController {

    @RequestMapping("/server")
    public @ResponseBody
    ServerConfVO getConfig() {
        final ServerConfVO serverConfVO = new ServerConfVO();
        serverConfVO.setRunStatus("");
        serverConfVO.setMode(0);
        serverConfVO.setModeLabel("");
        serverConfVO.setCrawlerCount(1);
        MonitorProtocol monitorProtocol = new MonitorProtocol();
        try {
            monitorProtocol = (MonitorProtocol) sendRequest(monitorProtocol);
            CrawlerData crawlerData = monitorProtocol.getMonitorData().getServerConfig();
            serverConfVO.setMode(crawlerData.getMode());
            serverConfVO.setModeLabel(crawlerData.getModeLabel());
            serverConfVO.setCrawlerCount(crawlerData.getCrawlerCount());
            serverConfVO.setRunStatus(crawlerData.getRunStatus());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return serverConfVO;
    }

    @RequestMapping("/clients")
    public
    @ResponseBody
    Collection<ClientInfo> getCrawlerClients() {
        List<ClientInfo> clientDataList = null;
        MonitorProtocol monitorProtocol = new MonitorProtocol();
        try {

            monitorProtocol = (MonitorProtocol) sendRequest(monitorProtocol);
            clientDataList = monitorProtocol.getMonitorData().getClientsConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clientDataList;
    }
}
